
var facebookTrack = {
    init: function () {
      setTimeout(function () {
        facebookTrack.createFbq();
      }, 1000)
    },
  
    formatPrice: function (number, decimals, dec_point, thousands_sep, symbol) {
      if (!number || !decimals || !dec_point || !thousands_sep || !symbol) return;
  
      number = (number + '').replace(',', '').replace(' ', '');
  
      let n = !isFinite(+number) ? 0 : +number,
        prec = !isFinite(+decimals) ? 0 : Math.abs(decimals),
        sep = (typeof thousands_sep === 'undefined') ? ',' : thousands_sep,
        dec = (typeof dec_point === 'undefined') ? '.' : dec_point,
        s = '',
        toFixedFix = function (n, prec) {
          let k = Math.pow(10, prec);
          return '' + Math.round(n * k) / k;
        };
  
      s = (prec ? toFixedFix(n, prec) : '' + Math.round(n)).split('.');
      if (s[0].length > 3) {
        s[0] = s[0].replace(/\B(?=(?:\d{3})+(?!\d))/g, sep);
      }
  
      if ((s[1] || '').length < prec) {
        s[1] = s[1] || '';
        s[1] += new Array(prec - s[1].length + 1).join('0');
      }
  
      return symbol + "" + s.join(dec);
    },
  
    createFbq: function () {
        var data = JSON.parse(sessionStorage.getItem('facebookTrack'));
  
        fbq('track', 'Purchase', {
          num_items: data.num_items,
          content_name: data.content_name,
          content_ids: data.content_ids,
          content_type: 'product',
          currency: 'BRL',
          value: data.value
        });

        sessionStorage.removeItem("facebookTrack");
    }
}

$(window).on('hashchange', function (e) {
    facebookTrack.init();
});

$(document).ready(function (e) {
    facebookTrack.init();
});
