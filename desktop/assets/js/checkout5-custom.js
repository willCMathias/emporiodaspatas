var fns = {
  formObject: function ($form) {
    var obj = {};
    $form.find("input#client-email").each(function () {
      var value;
      var name;
      if ($(this).attr("type") == "checkbox" && $(this).is(":checked")) {
        value = $(this).val();
        name = $(this).attr("name");
      }
      if ($(this).attr("type") == "option" && $(this).is(":checked")) {
        value = $(this).val();
        name = $(this).attr("name");
      }
      if ($(this).attr("type") != "option" && $(this).attr("type") != "checkbox") {
        value = $(this).val();
        name = $(this).attr("name");
      }
      obj[name] = value;
    });
    $form.find("select#pet").each(function () {
      var value = $(this).find("option:selected").val();
      var name = $(this).attr("name");
      obj[name] = value;
    });
    return obj;
  },
  getMasterData: function (url) {
    return $.ajax({
      headers: {
        Accept: "application/vnd.vtex.ds.v10+json",
        "Content-Type": "application/json",
        "REST-Range": "resources=0-1"
      },
      type: "GET",
      url: url
    });
  },
  saveMasterDate: function (entidade, data, sucessoMsg, form, alert) {
    if (sucessoMsg == null || sucessoMsg == undefined) {
      sucessoMsg = "Dados salvos com sucesso!";
    }
    $.ajax({
      url: "https://api.vtexcrm.com.br/emporiodaspatas/dataentities/" + entidade + "/documents/",
      dataType: "json",
      type: "PATCH",
      crossDomain: true,
      data: JSON.stringify(data),
      headers: {
        Accept: "application/vnd.vtex.ds.v10+json",
        "Content-Type": "application/json; charset=utf-8"
      },
      success: function (dataJson) {
        if (alert) {
          //alert(sucessoMsg);
        }
        if (form.size()) {
          form.each(function () {
            this.reset();
          });
        }
      }
    });
  },
  validaEmail: function (email) {
    var re = /^[-a-z0-9~!$%^&*_=+}{\'?]+(\.[-a-z0-9~!$%^&*_=+}{\'?]+)*@([a-z0-9_][-a-z0-9_]*(\.[-a-z0-9_]+)*\.(aero|arpa|biz|com|coop|edu|gov|info|int|mil|museum|name|net|org|pro|travel|mobi|[a-z][a-z])|([0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}))(:[0-9]{1,5})?$/i;
    return re.test(email);
  },
  newsletter: function ($form) {
    $form.on("submit", function (e) {
      e.preventDefault();
      var _self = $(this);
      var fields = fns.formObject($form);
      if (fns.validaEmail(fields.email)) {
        $.ajax({
          headers: {
            Accept: "application/vnd.vtex.ds.v10+json",
            "Content-Type": "application/json",
            "REST-Range": "resources=0-1"
          },
          type: "GET",
          url: "https://api.vtexcrm.com.br/emporiodaspatas/dataentities/NL/search?_fields=email&_where=email=" + fields.email
        }).done(function (clientInfo) {
          if (clientInfo.length == 0) {
            fns.saveMasterDate("NL", fields, null, _self, false);
            $("#newsletter-modal .active").removeClass("active");
            $("#newsletter-modal .success").addClass("active");
          } else {
            //alert("E-mails já restá cadastrado!");
            $("#newsletter-modal .active").removeClass("active");
            $("#newsletter-modal .error").addClass("active");
          }
        });
      }
    });
  }
};

var global = {
  newsletter: function () {
    fns.newsletter($("form.form-step.box-edit"));
  },
  init: function () {
    global.selectDog();
    global.newsletter();
  },
  selectDog: function () {
    $('select#pet').remove();
    $('form .box-client-info-pf').after('<select id="pet" name="pet" required="required"><option value="" selected="selected" disabled="disabled">Qual é o seu pet?</option><option value="Cachorro">Cachorro</option><option value="Gato">Gato</option><option value="Pássaro">Pássaro</option></select>');
  },
  cartTitle: function () {

  },
  textoBoleto: function () {
    var dataName = $('.payment-group-item[data-name="Boleto Bancário"]').hasClass('active');
    if (dataName && !$('.box-payment-option .payment-description .newtxt').length){
      var newTxt = `<span class="newtxt"><br /><strong style="font-weight:600;">O pedido realizado via boleto bancário será enviado após confirmação de pagamento do banco, 
      prazo de até 72 horas. SE VOCÊ PRECISA DO PRODUTO COM URGÊNCIA ESCOLHA OUTRO MÉTODO DE PAGAMENTO</strong></span>`;
      $('.box-payment-option').each(function(){
        if ( $(this).find('h3').text() === "Boleto bancário"){
          $(this).find('.payment-description').append(newTxt);
        }
      });
    }
  }
};

var facebookTrack = {
  init: function () {
    setTimeout(function () {
      if (vtexjs.checkout.orderForm.items.length){
        facebookTrack.createFbq();
      }
    }, 1000)
  },

  formatPrice: function (number, decimals, dec_point, thousands_sep, symbol) {
    if (!number || !decimals || !dec_point || !thousands_sep || !symbol) return;

    number = (number + '').replace(',', '').replace(' ', '');

    let n = !isFinite(+number) ? 0 : +number,
      prec = !isFinite(+decimals) ? 0 : Math.abs(decimals),
      sep = (typeof thousands_sep === 'undefined') ? ',' : thousands_sep,
      dec = (typeof dec_point === 'undefined') ? '.' : dec_point,
      s = '',
      toFixedFix = function (n, prec) {
        let k = Math.pow(10, prec);
        return '' + Math.round(n * k) / k;
      };

    s = (prec ? toFixedFix(n, prec) : '' + Math.round(n)).split('.');
    if (s[0].length > 3) {
      s[0] = s[0].replace(/\B(?=(?:\d{3})+(?!\d))/g, sep);
    }

    if ((s[1] || '').length < prec) {
      s[1] = s[1] || '';
      s[1] += new Array(prec - s[1].length + 1).join('0');
    }

    return symbol + "" + s.join(dec);
  },

  createFbq: function () {
    vtexjs.checkout.getOrderForm().done(function (orderForm) {
      console.log(orderForm);

      let listSku = orderForm.items;
      let arrSku = []
      let arrName = []

      listSku.forEach(function (i, v) {
        arrSku.push(i.id)
        arrName.push(i.name)
      })

      var totalPrice = $('.body-order-form .orderform-template .cart-template.mini-cart table tfoot .monetary').text() || $('.body-cart .full-cart .summary-totalizers tfoot .monetary').text();
      totalPrice = totalPrice.replace('R$ ', '').replace(',', '.');

      var listObject = {
        'num_items': listSku.length,
        'content_name': arrName,
        'content_ids': arrSku,
        'content_type': 'product',
        'value': totalPrice,
        'currency': 'BRL'
      }

      sessionStorage.removeItem("facebookTrack");
      sessionStorage.setItem('facebookTrack', JSON.stringify(listObject));

      fbq('track', 'InitiateCheckout', {
        num_items: listSku.length,
        content_name: arrName,
        content_ids: arrSku,
        content_type: 'product',
        value: totalPrice,
        currency: 'BRL'
      });

    });

  }
}

$(window).on('hashchange', function (e) {
  if (e.originalEvent.newURL.indexOf("/checkout/#/profile") > -1) {
    global.init();
  }
  if (e.originalEvent.newURL.indexOf("/checkout/#/payment") > -1) {
    global.textoBoleto();
  }
  $('.payment-group-item[data-name="Boleto Bancário"]').click(function(){
    global.textoBoleto();
  })
  facebookTrack.init();
});

$(document).ready(function () {
  $('h1#cart-title').after('<span class="btn-place-order-wrapper __hib-button"><a href="#/orderform" target="_self" data-event="cartToOrderform" id="cart-to-orderform" class="btn btn-large btn-success pull-left-margin btn-place-order" data-i18n="cart.finalize" data-bind="click: goToFirstInvalidStep">Fechar pedido</a></span>');
});


$(window).load(function () {
  global.textoBoleto();
  $('.payment-group-item[data-name="Boleto Bancário"]').click(function(){
    global.textoBoleto();
  })
});