const pdp = $("body").hasClass("product");
const pcategory = $("body").hasClass("department") || $("body").hasClass("category");

let productDatalayer = {
    init: function () {
        this.addCart();
        this.dataLayerSku();
    },

    dataLayerSku: function () {
        let disp = !skuJson.skus[0].available ? 'indisponivel' : 'disponivel';
        let skuID = skuJson.skus[0].sku;
        let listSku = skuJson.skus;
        let arrSku = [];
        let skuName = skuJson.skus[0].skuname;
        let bestPrice = skuJson.skus[0].bestPriceFormated
        bestPrice = bestPrice.replace('R$ ', '').replace(',', '.');

        listSku.forEach(function (i, v) {
            arrSku.push(i.sku)
        })

        $.ajax({
            url: 'https://emporiodaspatas.myvtex.com/api/catalog_system/pub/products/search?fq=skuId:' + skuID
        }).then(function (data) {
            let qtd = data[0].items[0].sellers[0].commertialOffer.AvailableQuantity;
            window.dataLayer = window.dataLayer || [];

            window.dataLayer.push({
                'event': 'productView',
                'skuID': arrSku,
                'disponibilidade': disp,
                'quantidadeDisponivel': qtd,
                'skuName': skuName
            });

            window.dataLayer.push({
                'event': 'skuID',
                'skuID': arrSku
            });

            window.dataLayer.push({
                'event': 'disponibilidade',
                'disponibilidade': disp,
            });

            window.dataLayer.push({
                'event': 'quantidadeDisponivel',
                'quantidadeDisponivel': qtd,
            });

            window.dataLayer.push({
                'event': 'skuName',
                'skuName': skuName
            });
        })
    },

    addCart: function name(params) {
        let listSku = skuJson.skus;
        let skuName = skuJson.skus[0].skuname;
        let bestPrice = '';

        listSku.forEach(function (i, v) {
            if (i.available){
                bestPrice = i.bestPriceFormated;
                bestPrice = bestPrice.replace('R$ ', '').replace(',', '.');
            }
        })

        $('.buy-button.buy-button-ref').click(function () {
            var url = $(this).attr('href');
            if (url != "javascript:alert('Por favor, selecione o modelo desejado.');") {
                url = url.replace('/checkout/cart/add?', '').replace('&seller=1&redirect=true&sc=1', '').replace('sku=', '|').replace('&qty=', '|');
                url = url.split('|')

                let dataLayerObj = {
                    'event': 'Click AddCart',
                    'products': skuName,
                    'skuID': url[1],
                    'quantidade': url[2],
                    'value': bestPrice
                }

                window.dataLayer = window.dataLayer || [];
                window.dataLayer.push(dataLayerObj);

                fbq('track', 'AddToCart', {
                    content_name: skuName,
                    content_category: vtxctx.categoryName,
                    content_ids: [url[1]],
                    content_type: 'product',
                    value: bestPrice,
                    currency: 'BRL'
                });
            }
        })
    },

    facebookTrack: function () {
        let listSku = skuJson.skus;
        let arrSku = []
        let skuName = skuJson.skus[0].skuname;
        let bestPrice = skuJson.skus[0].bestPriceFormated
        bestPrice = bestPrice.replace('R$ ', '').replace(',', '.');

        listSku.forEach(function (i, v) {
            arrSku.push(i.sku)
        })

        fbq('track', 'ViewContent', {
            value: bestPrice,
            currency: 'BRL',
            content_name: skuName,
            content_ids: arrSku,
            content_type: 'product'
        });
    }
}

let categoryFbq = {
    init: function () {
        this.trackCustomFacebook();
    },

    trackCustomFacebook: function () {
        fbq('trackCustom', 'ViewCategory', {
            content_category: vtxctx.categoryName,           
            content_type: 'product'
        });
    }
}

$(document).ready(function () {
    if (pdp) {
        productDatalayer.init();
        setTimeout(function() {
            productDatalayer.facebookTrack();
        }, 1250)
    }

});

$(window).load(function () {
    if (pcategory) {
        setTimeout(function() {
            categoryFbq.init();
        }, 1000)
    }  
      
});