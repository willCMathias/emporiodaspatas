
// Variaveis
var gulp = require('gulp'),
    cssmin = require('gulp-cssmin'),
    babel = require('gulp-babel'),
    uglify = require('gulp-uglify');

// CSSmin
gulp.task('cssmin', function () {
    // Desktop
    gulp.src('desktop/assets/css/*.css')
        .pipe(cssmin())
        .pipe(gulp.dest('dist/desktop/css'));

    // Mobile
    gulp.src('mobile/assets/css/*.css')
        .pipe(cssmin())
        .pipe(gulp.dest('dist/mobile/css'));
});

//JSmin
gulp.task('jsmin', function () {
    // Desktop
    gulp.src('desktop/assets/js/*.js')
        .pipe(babel({
            presets: ['@babel/env']
        }))
        .pipe(uglify())
        .pipe(gulp.dest('dist/desktop/js'));

    // Mobile
    gulp.src('mobile/assets/js/*.js')
        .pipe(babel({
            presets: ['@babel/env']
        }))
        .pipe(uglify())
        .pipe(gulp.dest('dist/mobile/js'));
});

// Chamada
gulp.task('default', function () {
    // Dektop
    gulp.watch('desktop/assets/css/*.css', ['cssmin']);
    gulp.watch('desktop/assets/js/*.js', ['jsmin']);

    // Mobile
    gulp.watch('mobile/assets/css/*.css', ['cssmin']);
    gulp.watch('mobile/assets/js/*.js', ['jsmin']);
});